#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "-3 -6\n"
        i, j = collatz_read(s)
        self.assertEqual(i, -3)
        self.assertEqual(j, -6)
    
    def test_read_3(self):
        s = "12397 56120\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 12397)
        self.assertEqual(j, 56120)

    # ----
    # eval
    # ----

    # test first if statement
    def test_eval_1(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    # test max_under and max_over
    def test_eval_2(self):
        v = collatz_eval(800, 2500)
        self.assertEqual(v, 209)

    # testing for loop
    def test_eval_3(self):
        v = collatz_eval(1000, 3000)
        self.assertEqual(v, 217)

    # testing mod statements
    def test_eval_4(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1.0, 10.0, 20.0)
        self.assertEqual(w.getvalue(), "1.0 10.0 20.0\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, "1", "10", "20")
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n")
    
    def test_solve_3(self):
        r = StringIO("20 30\n50 70\n100 180\n50 70\n200 400\n300 500\n473 822\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "20 30 112\n50 70 113\n100 180 125\n50 70 113\n200 400 144\n300 500 144\n473 822 171\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
