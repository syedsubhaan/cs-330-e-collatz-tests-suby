#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "5 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 5)
    
    def test_read_3(self):
        s = "3 400\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3)
        self.assertEqual(j, 400)

    def test_read_4(self):
        s = "69993 3256782687\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 69993)
        self.assertEqual(j, 3256782687)
    
    def test_read_5(self):
        s = "888 777\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 888)
        self.assertEqual(j, 777)
    

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    def test_eval_6(self):
        v = collatz_eval(850, 950)
        self.assertEqual(v, 179)
    
    def test_eval_7(self):
        v = collatz_eval(78465, 78466)
        self.assertEqual(v, 139)
    
    def test_eval_8(self):
        v = collatz_eval(999999, 1000000)
        self.assertEqual(v, 259)
    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1000, 900, 174)
        self.assertEqual(w.getvalue(), "1000 900 174\n")
    
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 78465, 78466, 139)
        self.assertEqual(w.getvalue(), "78465 78466 139\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_2(self):
        r = StringIO("1 10\n201 210\n850 950\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n201 210 89\n850 950 179\n1000 900 174\n")
    
    def test_solve_3(self):
        r = StringIO("78465 78466\n170000 180000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "78465 78466 139\n170000 180000 347\n")
    
    def test_solve_4(self):
        r = StringIO("45672 46728\n675 934\n4 235\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "45672 46728 283\n675 934 179\n4 235 128\n")
    
    def test_solve_5(self):
        r = StringIO("643 235\n333 333\n3426 6742\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "643 235 144\n333 333 113\n3426 6742 262\n")
    

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
