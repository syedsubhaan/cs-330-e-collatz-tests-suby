#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "999999 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 100)

    def test_read_3(self):
        s = "100 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 100)

    def test_read_4(self):
        s = "1 999000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 999000)

    def test_read_5(self):
        s = "1000 10000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1000)
        self.assertEqual(j, 10000)

    def test_read_6(self):
        s = "12 99\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 12)
        self.assertEqual(j, 99)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(99999, 9999)
        self.assertEqual(v, 351)

    def test_eval_7(self):
        v = collatz_eval(1, 2000)
        self.assertEqual(v, 182)

    def test_eval_8(self):
        v = collatz_eval(2000, 1)
        self.assertEqual(v, 182)

    def test_eval_9(self):
        v = collatz_eval(436809, 436182)
        self.assertEqual(v, 325)

    def test_eval_10(self):
        v = collatz_eval(929349, 929040)
        self.assertEqual(v, 339)

    def test_eval_11(self):
        v = collatz_eval(1000, 984)
        self.assertEqual(v, 112)

    def test_eval_12(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_13(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)






    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 10, 25)
        self.assertEqual(w.getvalue(), "100 10 25\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 1)
        self.assertEqual(w.getvalue(), "1 999999 1\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1000, 1000, 2000)
        self.assertEqual(w.getvalue(), "1000 1000 2000\n")

    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 999999, 999999, 999999)
        self.assertEqual(w.getvalue(), "999999 999999 999999\n")

    def test_print_7(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve_3(self):
        r = StringIO("5 100\n100 400\n1101 2222\n330 331\n2 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 100 119\n100 400 144\n1101 2222 182\n330 331 113\n2 1 2\n")

    def test_solve_4(self):
        r = StringIO("1 10000\n999 99")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10000 262\n999 99 179\n")

    def test_solve_5(self):
        r = StringIO("155281 158691\n1 1\n 500 101\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "155281 158691 383\n1 1 1\n500 101 144\n")

    def test_solve_6(self):
        r = StringIO("1000 900\n99999 9999\n9999 9998\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 900 174\n99999 9999 351\n9999 9998 92\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
